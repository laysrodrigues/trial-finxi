# Trial Finxi - MVP

[![pipeline status](https://invent.kde.org/laysrodrigues/trial-finxi/badges/master/pipeline.svg)](https://invent.kde.org/laysrodrigues/trial-finxi/commits/master)
[![coverage report](https://invent.kde.org/laysrodrigues/trial-finxi/badges/master/coverage.svg)](https://invent.kde.org/laysrodrigues/trial-finxi/commits/master)

## How to Run

### Requirements

1) Clone this repository
2) Create your virtualenv with Python 3.6

#### For Development

1) Install this app
    `pip install -e .`
2) Configure your local environment vars based on the file `.env.example` and run:
    `source .env`

#### For Production

1) Setup an environment with Docker and Docker-Compose
2) Move to the folder of this application
3) Run the following commands:
    ```bash
    $ docker-compose build
    $ docker-compose up
    ```

The above command will run this app and the database inside docker containers.

#### Postman

- Note: Please set an environment variable called `token` and call the route of Authentication
before testing other routes. I hope that the collection is correct for your use.

## Challenges

- Learn how postman works, I never used before(I'm more a curl girl), and it's a great tool inded.
- Learn docker-compose, I already knew docker, but never got the chance to use compose.
- Use JWT for authentication, more on lessons learned.
- Try to write the App with TDD, more on lessons learned.
- Try(And fail) to change the icon on Django Admin. I do think that is possible using blocks as I saw on the docs, but I wasn't able to find anywhere what I needed to do to change that field. I just didn't wanted to try to inject some css on it because it would be a ugly solution.

## Lessons learned

- Trying to write the views with TDD is hard. I am not used to develop with TDD, and my experience with
web development is minimum. So using Django and DRF was good for me to see how much the technology of Django evolved
since I made the Welcome to the Django course back in 2016. On that course Henrique used TDD, so I tried my best to
remember what he taugth so I could use on this app.
- Use JWT for authentication and fix tests. Until I realized that I just needed a way to authenticate the user via a json header it took me a while. And thanks to the DRF documentation I was able to figure out JWT and was easy to implement it. I just don't know if the approach that I used was the best on the tests.
- Last but not least, thank for this opportunity, I am glad that I was able to finish this test and that I was able to do almost everything out of the box.
