FROM python:3.6
RUN apt update && apt upgrade -y
ENV PYTHONUNBUFFERED 1
WORKDIR /usr/src/app
COPY . .
RUN pip install --cache-dir=.pip -e .