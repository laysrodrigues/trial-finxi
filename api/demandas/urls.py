from django.conf.urls import url
from django.urls import path

from . import views as demands_views

app_name = 'demands'
urlpatterns = [
    url(
        r'^$',
        demands_views.get_post_handler,
        name='get_post_handler',
    ),
    url(r'^(?P<id>\d+)/$',
        demands_views.delete_patch_handler,
        name='delete_patch_handler'),
    url(r'^(?P<id>\d+)/close',
        demands_views.close_demand_handler,
        name='close'),
]
