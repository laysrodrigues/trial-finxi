from django.contrib import admin
from .models import DemandsModel


class DemandsModelAdmin(admin.ModelAdmin):
    list_display = (
        'advertiser',
        'email',
        'phone_number',
        'deliver_address',
        'city',
        'state',
        'zip_code',
        'status',
        'part_description',
        'created_at',
    )


admin.site.register(DemandsModel, DemandsModelAdmin)