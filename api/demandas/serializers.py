from rest_framework import serializers
from .models import DemandsModel


class DemandsSerializer(serializers.ModelSerializer):
    """Serializar for Demands Model."""

    class Meta:
        model = DemandsModel
        fields = '__all__'