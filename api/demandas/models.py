from django.db import models
from phone_field import PhoneField
# Create your models here.


class DemandsModel(models.Model):
    """Model of the demands."""

    part_description = models.CharField('Descrição da Peça', max_length=200)
    deliver_address = models.CharField('Endereço de Entrega', max_length=150)
    city = models.CharField('Cidade', max_length=20)
    state = models.CharField('Estado', max_length=20)
    zip_code = models.CharField('CEP', max_length=9)
    phone_number = PhoneField('Telefone', max_length=16)
    email = models.EmailField('E-mail')
    advertiser = models.CharField('Anunciante', max_length=50)
    status = models.BooleanField('Status de Finalização', default=False)
    created_at = models.DateTimeField('Criado em', auto_now_add=True)

    class Meta:
        verbose_name = 'demanda'
        verbose_name_plural = 'demandas'
        ordering = ('-created_at', )
