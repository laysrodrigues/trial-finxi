from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from .models import DemandsModel
from .serializers import DemandsSerializer


@api_view(['GET', 'POST'])
@permission_classes((IsAuthenticated, ))
def get_post_handler(request):
    """List demands or create a new demand."""
    if request.method == 'POST':
        return create(request)
    return all(request)


def create(request):
    """Create a new demand."""
    data = JSONParser().parse(request)
    serializer = DemandsSerializer(data=data)
    if serializer.is_valid():
        serializer.create(validated_data=serializer.data)
        return Response(status=status.HTTP_201_CREATED)
    return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def all(request):
    """Return all demands."""
    demands = []
    if request.user.is_superuser:
        demands = DemandsModel.objects.all()
    else:
        demands = DemandsModel.objects.filter(advertiser=request.user)
    if demands.count() > 0:
        serializer = DemandsSerializer(demands, many=True)
        if serializer:
            return Response(data=serializer.data, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['DELETE', 'PATCH'])
@permission_classes((IsAuthenticated, ))
def delete_patch_handler(request, id):
    if request.method == "DELETE":
        return delete(request, id)
    return update_demand(request, id)


# @api_view(['DELETE'])
def delete(request, id):
    """Delete a demand."""
    demand = get_object_or_404(DemandsModel, id=id)
    demand.delete()
    return Response(status=status.HTTP_200_OK)


# @api_view(['PATCH'])
def update_demand(request, id):
    """Update demands any fields."""
    data = JSONParser().parse(request)
    demand = get_object_or_404(DemandsModel, id=id)
    serializer = DemandsSerializer(demand, data=data, partial=True)
    if serializer.is_valid():
        serializer.save()
        return Response(status=status.HTTP_200_OK)
    return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['PATCH'])
def close_demand_handler(request, id):
    """Update the status of a demand to closed."""
    if DemandsModel.objects.filter(id=id).update(status=True):
        return Response(status=status.HTTP_200_OK)
    return Response(status=status.HTTP_404_NOT_FOUND)
