import json
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from api.demandas.models import DemandsModel


class DemandCreateTest(APITestCase):
    """Test View Demands."""

    def setUp(self):
        self.user = User.objects.create_user(username='test',
                                             email='test@test.com',
                                             password='password123')
        url = reverse('token_obtain_pair')
        response = self.client.post(url, {
            "username": "test",
            "password": "password123"
        })
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                                response.data['access'])

    def test_create_demand_success(self):
        """Ensure we can create a new demand."""
        url = reverse('demands:get_post_handler')
        data = {
            "advertiser": "Test Advertiser",
            "email": "test@federation.org",
            "phone_number": "21 987654321",
            "deliver_address": "R2D2 Lab",
            "city": "Niterói",
            "state": "Rio de Janeiro",
            "zip_code": "24210-110",
            "part_description": "Some shiny thing"
        }

        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(DemandsModel.objects.count(), 1)
        self.assertEqual(DemandsModel.objects.get().advertiser,
                         'Test Advertiser')

    def test_create_demand_missing_field(self):
        """Ensure we can't create a new demand with missing field."""
        url = reverse('demands:get_post_handler')
        data = {
            "advertiser": "Test Advertiser",
            "email": "test@federation.org",
            "phone_number": "21 987654321",
            "deliver_address": "R2D2 Lab",
            "city": "Niterói",
            "state": "Rio de Janeiro",
            "zip_code": "24210-110",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_demand_bad_format(self):
        """Ensure we can't create a new demand with bad format."""
        url = reverse('demands:get_post_handler')
        data = data = {
            "advertiser": "Test Advertiser",
            "email": "test@federation.org",
            "phone_number": "+55 21 9876543210",
            "deliver_address": "R2D2 Lab",
            "city": "Niterói",
            "state": "Rio de Janeiro",
            "zip_code": "24210-110",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class DemandsListSuccessTest(APITestCase):
    def setUp(self):
        self.obj1 = DemandsModel.objects.create(
            advertiser="test",
            email="test@federation.org",
            phone_number="21 987654321",
            deliver_address="R2D2 Lab",
            city="Niterói",
            state="Rio de Janeiro",
            zip_code="24210-110",
            part_description="Some shiny thing",
        )
        self.obj2 = DemandsModel.objects.create(
            advertiser="r2d2",
            email="test@federation.org",
            phone_number="21 987654321",
            deliver_address="R2D2 Lab",
            city="Niterói",
            state="Rio de Janeiro",
            zip_code="24210-110",
            part_description="Some shiny thing",
        )
        self.admin = User.objects.create_superuser(username='admin',
                                                   email='admin@admin.com',
                                                   password='password123')

        self.user = User.objects.create_user(username='test',
                                             email='test@test.com',
                                             password='password123')

    def test_list_all(self):
        """Ensure that only admin can list all demands."""
        url = reverse('token_obtain_pair')
        response = self.client.post(url, {
            "username": "admin",
            "password": "password123"
        })
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                                response.data['access'])
        url = reverse('demands:get_post_handler')
        response = self.client.get(url)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_list_user_only(self):
        """Ensure that an advertiser can list its own demands."""
        url = reverse('token_obtain_pair')
        response = self.client.post(url, {
            "username": "test",
            "password": "password123"
        })
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                                response.data['access'])
        url = reverse('demands:get_post_handler')
        response = self.client.get(url)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class DemandListFailTest(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='test',
                                             email='test@test.com',
                                             password='password123')
        url = reverse('token_obtain_pair')
        response = self.client.post(url, {
            "username": "test",
            "password": "password123"
        })

        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                                response.data['access'])

    def test_list_not_found(self):
        """Ensure that we get 404 if the db is empty."""
        url = reverse('demands:get_post_handler')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class DemandsDeleteSuccessTest(APITestCase):
    def setUp(self):
        self.obj = DemandsModel.objects.create(
            advertiser="Test Advertiser",
            email="test@federation.org",
            phone_number="21 987654321",
            deliver_address="R2D2 Lab",
            city="Niterói",
            state="Rio de Janeiro",
            zip_code="24210-110",
            part_description="Some shiny thing",
        )
        self.user = User.objects.create_user(username='test',
                                             email='test@test.com',
                                             password='password123')
        url = reverse('token_obtain_pair')
        response = self.client.post(url, {
            "username": "test",
            "password": "password123"
        })

        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                                response.data['access'])

    def test_delete_success(self):
        """Ensure that we can delete a demand."""
        url = reverse('demands:delete_patch_handler', kwargs={'id': 1})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class DemandsCloseTest(APITestCase):
    def setUp(self):
        self.obj = DemandsModel.objects.create(
            advertiser="Test Advertiser",
            email="test@federation.org",
            phone_number="21 987654321",
            deliver_address="R2D2 Lab",
            city="Niterói",
            state="Rio de Janeiro",
            zip_code="24210-110",
            part_description="Some shiny thing",
        )
        self.user = User.objects.create_user(username='test',
                                             email='test@test.com',
                                             password='password123')
        url = reverse('token_obtain_pair')
        response = self.client.post(url, {
            "username": "test",
            "password": "password123"
        })

        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                                response.data['access'])


#

    def test_close_demand(self):
        """Ensure that we can update the status of a demand."""
        url = reverse('demands:close', kwargs={'id': 1})
        response = self.client.patch(url)
        self.obj.refresh_from_db()
        self.assertTrue(self.obj.status)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_patch_demand_fail(self):
        """Ensure that we don't update non existent demand."""
        url = reverse('demands:close', kwargs={'id': 2})
        response = self.client.patch(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class DemandsUpdateSuccessTest(APITestCase):
    def setUp(self):
        self.obj = DemandsModel.objects.create(
            advertiser="Test Advertiser",
            email="test@federation.org",
            phone_number="21 987654321",
            deliver_address="R2D2 Lab",
            city="Niterói",
            state="Rio de Janeiro",
            zip_code="24210-110",
            part_description="Some shiny thing",
        )
        self.user = User.objects.create_user(username='test',
                                             email='test@test.com',
                                             password='password123')
        url = reverse('token_obtain_pair')
        response = self.client.post(url, {
            "username": "test",
            "password": "password123"
        })

        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                                response.data['access'])

    def test_update(self):
        """Ensure that we can update a demand."""
        data = {
            "city": "Rio de Janeiro",
            "part_description": "Some shiny new thing"
        }
        url = reverse('demands:delete_patch_handler', kwargs={'id': 1})
        response = self.client.patch(url, data)
        self.obj.refresh_from_db()
        self.assertEqual(self.obj.city, "Rio de Janeiro")
        self.assertEqual(self.obj.part_description, "Some shiny new thing")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
