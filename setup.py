from setuptools import setup, find_packages

setup(
    name='droids_quotation_api',
    author='Lays Rodrigues',
    author_email='lays.rodrigues@kde.org',
    description='MVP for Finxi Trial, an API for quotation of Droids',
    packages=find_packages(exclude=('tests')),
    install_requires=[
        'Django==2.2.1',
        'djangorestframework==3.9.4',
        'dj-database-url==0.5.0',
        'psycopg2==2.8.2',
        'django-phone-field==1.7.1',
        'djangorestframework_simplejwt==4.3.0',
        'django-nose==1.4.6',
        'coverage==4.5.3',
    ],
    extras_require={
        'dev': [
            'yapf==0.27.0',
            'pylint==2.3.1',
            'ipython==7.5.0',
        ],
    },
)
